"use strict";

module.exports = function (sequelize, Sequelize, DataTypes) {
    var Category = sequelize.define("Category", {
        id: {
            field: "id",
            primaryKey: true,
            type: Sequelize.STRING(255),
            allowNull: false
        },
        name: {
            field: "name",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        hint: {
            field: "hint",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        createdAt: {
            field: "createdAt",
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        timestamps: false
    });

    Category.associate = function (models) {
        Category.hasMany(models.Question, {
            foreignKey: {
                name: 'categoryId',
                allowNull: false
            }, as: 'questions',
            cascade: true,
            onDelete: 'cascade',
            onUpdate: 'cascade'
        })
    };

    return Category;
};