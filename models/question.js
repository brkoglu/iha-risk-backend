"use strict";

module.exports = function (sequelize, Sequelize, DataTypes) {
    var Question = sequelize.define("Question", {
        id: {
            field: "id",
            primaryKey: true,
            type: Sequelize.STRING(255),
            allowNull: false
        },
        name: {
            field: "name",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        hint: {
            field: "hint",
            type: Sequelize.STRING(255),
            allowNull: true
        },
        createdAt: {
            field: "createdAt",
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        timestamps: false
    });

    Question.associate = function (models) {
        Question.hasMany(models.Option, {
            foreignKey: {
                name: 'questionId',
                allowNull: false
            }, as: 'options',
            cascade: true,
            onDelete: 'cascade',
            onUpdate: 'cascade'
        })
    };

    return Question;
};