"use strict";

module.exports = function (sequelize, Sequelize, DataTypes) {
    return sequelize.define("Modifier", {
        id: {
            field: "id",
            primaryKey: true,
            type: Sequelize.STRING(255),
            allowNull: false
        },
        targetQuestion: {
            field: "targetQuestion",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        createdAt: {
            field: "createdAt",
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        timestamps: false
    });
};