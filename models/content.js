"use strict";

module.exports = function (sequelize, Sequelize, DataTypes) {
    var Content = sequelize.define("Content", {
        id: {
            field: "id",
            primaryKey: true,
            type: Sequelize.STRING(255),
            allowNull: false
        },
        name: {
            field: "name",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        value: {
            field: "value",
            type: Sequelize.TEXT,
            allowNull: false
        },
        createdAt: {
            field: "createdAt",
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        timestamps: false
    });

    return Content;
};