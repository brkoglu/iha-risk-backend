"use strict";

module.exports = function (sequelize, Sequelize, DataTypes) {
    var Option = sequelize.define("Option", {
        id: {
            field: "id",
            primaryKey: true,
            type: Sequelize.STRING(255),
            allowNull: false
        },
        name: {
            field: "name",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        value: {
            field: "value",
            type: Sequelize.STRING(255),
            allowNull: false
        },
        factor: {
            field: "factor",
            type: Sequelize.DECIMAL(4, 4),
            allowNull: false
        },
        hint: {
            field: "hint",
            type: Sequelize.STRING(255),
            allowNull: true
        },
        createdAt: {
            field: "createdAt",
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        timestamps: false
    });

    Option.associate = function (models) {
        Option.hasMany(models.Modifier, {
            foreignKey: {
                name: 'optionId',
                allowNull: false
            }, as: 'modifiers',
            cascade: true,
            onDelete: 'cascade',
            onUpdate: 'cascade'
        })
    };

    return Option;
};