var models = require('../models');
var express = require('express');
var sequelize = require("sequelize");
var router = express.Router();
var sleep = require('system-sleep');
var sequelizeValues = require('sequelize-values')();

router.get('/', function (req, res) {
    res.render('index', {
        title: 'iha-risk-backend',
        apidoc: '/api/categories'
    });
});

router.get('/api/content/:name', function (req, res) {

    models.Content.findOne({
        where: {
            name: req.params.name
        }
    }).then(
        function (result) {
            if (result) {
                res.status(200);
                res.send(result);
            } else {
                res.status(404);
                res.send();
            }
        }
    ).catch(function (err) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
        res.json({'error': err.message});
    });
});

router.delete('/api/content/:name', function (req, res) {
    models.Content.destroy({
        where: {
            name: req.params.name
        }
    }).then(function (rowDeleted) {
        if (rowDeleted === 1) {
            res.status(200);
            res.send();
        } else {
            res.status(204);
            res.send();
        }
    }, function (err) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
        res.json({'error': err.message});
    });
});


router.post('/api/content', function (req, res) {

    models.Content.upsert({
        id: req.body.id,
        name: req.body.name,
        value: req.body.value,
        createdAt: req.body.createdAt
    }).then(
        function (result) {
            if (result) {
                res.status(201);
                res.send();
            } else {
                res.status(303);
                res.send();
            }
        }
    ).catch(function (err) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
        res.json({'error': err.message});
    });

});


router.get('/api/categories/:id', function (req, res) {
    models.Category.findById(req.params.id, {
        include: [{
            model: models.Question, as: 'questions',
            include: [{
                model: models.Option, as: 'options',
                include: [{
                    model: models.Modifier, as: 'modifiers'
                }]
            }]
        }]
    }).then(
        function (result) {
            if (result) {
                res.status(200);
                res.send(result);
            } else {
                res.status(404);
                res.send();
            }
        }
    ).catch(function (err) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
        res.json({'error': err.message});
    });
});


router.get('/api/categories', function (req, res) {
    models.Category.findAll({
        include: [{
            model: models.Question, as: 'questions',
            include: [{
                model: models.Option, as: 'options',
                include: [{
                    model: models.Modifier, as: 'modifiers'
                }]
            }]
        }]
    }).then(
        function (result) {
            if (result) {
                res.status(200);
                res.send(result);
            } else {
                res.status(404);
                res.send();
            }
        }
    ).catch(function (err) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
        res.json({'error': err.message});
    });
});


router.delete('/api/categories/:id', function (req, res) {
    models.Category.destroy({
        where: {
            id: req.params.id
        }
    }).then(function (rowDeleted) {
        if (rowDeleted === 1) {
            res.status(200);
            res.send();
        } else {
            res.status(204);
            res.send();
        }
    }, function (err) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
        res.json({'error': err.message});
    });
});


router.post('/api/categories', function (req, res) {
    var categories = [];
    req.body.forEach(function (item, index) {
        categories.push(item)
    });

    categories.forEach(function (category, index) {

            models.Category.destroy({
                where: {
                    id: category.id
                }
            }).then(sleep(200)).then(
                models.Category.upsert({
                        id: category.id,
                        name: category.name,
                        hint: category.hint,
                        createdAt: category.createdAt
                    }
                ));
            category.questions.forEach(function (question, index) {
                models.Question.destroy({
                    where: {
                        id: question.id
                    }
                }).then(sleep(200)).then(
                    models.Question.upsert({
                            id: question.id,
                            name: question.name,
                            hint: question.hint,
                            createdAt: question.createdAt,
                            categoryId: question.categoryId
                        }
                    ));
                question.options.forEach(function (option, index) {
                    models.Option.destroy({
                        where: {
                            id: option.id
                        }
                    }).then(sleep(200)).then(
                        models.Option.upsert({
                                id: option.id,
                                name: option.name,
                                value: option.value,
                                hint: option.hint,
                                factor: option.factor,
                                createdAt: option.createdAt,
                                questionId: option.questionId
                            }
                        ));
                    option.modifiers.forEach(function (modifier, index) {
                        models.Option.destroy({
                            where: {
                                id: modifier.id
                            }
                        }).then(sleep(200)).then(
                            models.Modifier.upsert({
                                id: modifier.id,
                                sourceId: modifier.sourceId,
                                targetQuestion: modifier.targetQuestion,
                                createdAt: modifier.createdAt,
                                optionId: modifier.optionId
                            }));
                    });
                });
            });
        }
    );
    res.status(200).json({
        message: 'categories successfully saved'
    });
});

module.exports = router;